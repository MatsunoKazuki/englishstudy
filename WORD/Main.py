#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import sqlite3
import time
import threading
from collections import OrderedDict
PATH = "./DB/"

# 問題選択用辞書
QUESTION_CHOICE_DICT = OrderedDict([('01', "日本語読みから英語を入力する"),
                                    ('02', "英語から日本語読みを選択する")])
# 問題出題方法選択辞書
QUESTION_FORMAT_DICT = OrderedDict([('01', "順番に最初から"),
                                    ('02', "ランダム")])

# 間違えた問題
mistake_answer = []

# 回答表示時間
answer_show_time_s = 5


# DBの辞書を取得する関数
def getDbList():
    db_list = OrderedDict()
    for x in os.listdir(PATH):
        if os.path.isfile(PATH + x):
            # 数字と文字を分割
            words = x.replace("_", " ").split(" ")
            db_list[words[0]] = words[1]

    return db_list


# リストを出力して選択させる関数
def showDbList(ordered_dict):
    while True:
        for key, value in sorted(ordered_dict.items()):
            print(key + "\t" + value)
        
        print("上記左記の数字を入力してね")
        select = input()
        
        # 入力値がリストに含まれるかの確認
        if select in ordered_dict.keys():
            return select
        else:
            print("ちゃんと書いてある数字いれてよ。")




# 問題スタート
def startQuestion(db_name, choice_id, format_id):
    connection          = sqlite3.connect(PATH + db_name)
    question_cursor = connection.cursor()
    answer_cursor   = connection.cursor()
    
    # ソート順設定
    if format_id == "01":
        # ソート順は順番に
        order_by    = "ORDER BY id"
    elif format_id == "02":
        # ソート順はランダムに
        order_by    = "ORDER BY RANDOM()"
    
    
    if choice_id == "01":
        # 現在出題数カウント用
        count = 0
    
        # 日本語読みから英語入力
        jp_select_sql       = "SELECT jp FROM Question GROUP BY JP {sort}";
        en_select_sql    = "SELECT en FROM Question WHERE jp = ?";
        for row in question_cursor.execute(jp_select_sql.format(sort=order_by)):
            count += 1
            jp_word = row[0]
            
            answer_cursor.execute(en_select_sql, [jp_word])
            answer_list = answer_cursor.fetchall()
            print()
            print("現在" + str(count) + "問目")
            print(jp_word + "->")
            thread1 = myThread()
            if len(answer_list) == 1:
                # 1個しか答えが無いもの
                # 英語入力
                thread1.setAnswer(answer_list[0])
                thread1.start()
                en_word = input()
                if en_word in answer_list[0]:
                    print("正解")
                    thread1.setAnswerFlag()
                else:
                    print("不正解")
                    print(answer_list[0])
                    mistake_answer.append(jp_word)
                    thread1.setAnswerFlag()
            else:
                list = []
                for x in answer_list:
                    list.append(x[0])

                # 複数個答えがあるもの
                for i in range(0, len(list)):
                    thread1 = myThread()
                    print(str(i + 1) + "個目の答えを入力")
                    # 英語入力
                    thread1.setAnswer(list)
                    thread1.start()
                    en_word = input()

                    if en_word in list:
                        print("正解")
                        list.remove(en_word)
                        thread1.setAnswerFlag()
                    else:
                        print("不正解")
                        print(list)
                        mistake_answer.append(jp_word)
                        thread1.setAnswerFlag()
                        break
        
    elif choice_id == "02":
        # 英語から日本語読み選択
        print("未実装")
    else:
        # なんか変じゃないか
        print("変だよね")
        exit




class myThread(threading.Thread):
    def __init__(self):
        super(myThread, self).__init__()
    
    def setAnswerFlag(self):
        self._answer_flag = True
    
    def setAnswer(self, value):
        self._answer = value
    
    def run(self):
        self._answer_flag = False
        time.sleep(answer_show_time_s)
        if self._answer_flag == False :
            print(self._answer)




if __name__ == '__main__':
    # 使用するDB名等を選択させる
    db_name_id = showDbList(getDbList())
    choice_id  = showDbList(QUESTION_CHOICE_DICT)
    format_id  = showDbList(QUESTION_FORMAT_DICT)


    print()
    print(getDbList()[db_name_id])
    print(QUESTION_CHOICE_DICT[choice_id])
    print(QUESTION_FORMAT_DICT[format_id])
    print()
    print("上記条件でスタート")

    startQuestion(db_name_id + "_" + getDbList()[db_name_id], choice_id, format_id)


    print(mistake_answer)
