import enum
from collections import OrderedDict
import os
import sqlite3
import time
import threading

class Choice(enum.Enum):
    # 日本語から英語に
    jp_to_en = '01'
    # 英語から日本語に
    en_to_jp = '02'
    
    def getChoiceList():
        choice_list = OrderedDict()
        choice_list[Choice.jp_to_en.value] = "日本語から英語に"
        choice_list[Choice.en_to_jp.value] = "英語から日本語に"
        return choice_list

class Order(enum.Enum):
    # 現状の順番どおりで最初から
    order_by_id = '01'
    # 英語名前順(昇順)
    order_by_en_asc = '02'
    # 英語名前順(降順)
    order_by_en_desc = '03'
    # 日本語名前順(昇順)
    order_by_jp_asc = '04'
    # 日本語名前順(降順)
    order_by_jp_desc = '05'
    # ランダム
    order_by_random = '06'
    
    def getOrderList():
        order_list = OrderedDict()
        order_list[Order.order_by_id.value]       = "順番に最初から"
        order_list[Order.order_by_en_asc.value]   = "英語名前順(昇順)"
        order_list[Order.order_by_en_desc.value]  = "英語名前順(降順)"
        order_list[Order.order_by_jp_asc.value]   = "日本語名前順(昇順)"
        order_list[Order.order_by_jp_desc.value]  = "日本語名前順(降順)"
        order_list[Order.order_by_random.value]   = "ランダム"
        return order_list


class Question():
    __JP_TO_EN_JP_SELECT_SQL = "SELECT jp FROM Question GROUP BY JP {order}"
    __JP_TO_EN_EN_SELECT_SQL = 'SELECT en FROM Question WHERE JP = "{jp}"'
    
    # 不正解時に同じ文字を入力させる回数
    __REPEAT_INPUT_COUNT = 3
    

    def __init__(self, db_connect, choice, order):
        self._choice = choice
        
        self._question_cursor   = db_connect.cursor()
        self._select_cursor     = db_connect.cursor()
        
        self._order = order

    def __getOrder(self):
        # ソート順
        if self._order == Order.order_by_id.value:
            order = "ORDER BY id"
        elif self._order == Order.order_by_en_asc.value:
            order = "ORDER BY en ASC"
        elif self._order == Order.order_by_en_desc.value:
            order = "ORDER BY en DESC"
        elif self._order == Order.order_by_jp_asc.value:
            order = "ORDER BY jp ASC"
        elif self._order == Order.order_by_jp_desc.value:
            order = "ORDER BY jp DESC"
        elif self._order == Order.order_by_random.value:
            order = "ORDER BY RANDOM()"
        # orderを返す
        return order
    
    

    def start(self):
        # 現在の出題数カウント
        count = 0
        
        # 間違った問題
        mistake_question = []
    
        # 問題選択
        if self._choice == Choice.jp_to_en.value:
            # 日本語から英語に
            for row in self._question_cursor.execute(self.__JP_TO_EN_JP_SELECT_SQL.format(order = self.__getOrder())):
                count += 1
                jp_word = row[0]
                
                self._select_cursor.execute(self.__JP_TO_EN_EN_SELECT_SQL.format(jp = jp_word))
                answer_list = self._select_cursor.fetchall()
                
                print()
                print("現在" + str(count) + "問目")
                print(jp_word + "->")
                
                
                if len(answer_list) == 1:
                    # 回答用スレッド作成
                    thread = myThread()
                    thread.setAnswer(answer_list[0])
                    thread.start()

                    # 答えが1この場合
                    
                    input_answer = input()
                    if input_answer in answer_list[0]:
                        print("正解")
                        # 正解でも不正解でもフラグをセットする
                        thread.setAnswerFlag()
                    else:
                        print("不正解")
                        print(answer_list[0])
                        mistake_question.append(jp_word)
                        # 正解でも不正解でもフラグをセットする
                        thread.setAnswerFlag()

                        self.__answerRepeatInput(jp_word, answer_list[0])
                else:
                    list = []
                    for x in answer_list:
                        list.append(x[0])
                    
                    # 複数答えがあるもの
                    for i in range(0, len(list)):
                        print(str(i + 1) + "個目の答えを入力")
                        # 回答用スレッド作成
                        thread = myThread()
                        thread.setAnswer(list)
                        thread.start()

                        # 英語入力
                        input_answer  = input()
                        
                        if input_answer in list:
                            print("正解")
                            list.remove(input_answer)
                            thread.setAnswerFlag()
                        else:
                            print("不正解")
                            print(list)
                            mistake_question.append(jp_word)
                            thread.setAnswerFlag()
                            
                            for i in list:
                                self.__answerRepeatInput(jp_word, i)
                            break

        elif self._choice == Choice.en_to_jp.value:
            print("未実装")
    
    
    def __answerRepeatInput(self, question, answer):
        i = 0
        while True:
            # iをインクリメント
            i += 1
            print(question + "->")
            input_answer = input()
            print(answer)
            print(input_answer)
            if input_answer in answer:
                print("正解")
            else:
                print("不正解")
                i -= 1
                print(i)
            # 繰り返し指定回数より多くなったら終了
            if i >= self.__REPEAT_INPUT_COUNT:
                break


class DB():
    __PATH = "./DB/"
    
    def getDbList(self):
        db_list = OrderedDict()
        # ディレクトリ検索
        for x in os.listdir(self.__PATH):
            # ファイルのみ取得する
            if os.path.isfile(self.__PATH + x):
                # 数字と文字を分割する
                num_and_word = x.split("_")
                num     = num_and_word[0]
                word    = num_and_word[1]
                db_list[num] = word
        return db_list
    
    # DBのコネクションを返す
    def getConnection(self, db_name):
        connect = sqlite3.connect(self.__PATH + db_name)
        return connect





class myThread(threading.Thread):
    __ANSWER_SHOW_TIME_S = 5
    
    def __init__(self):
        super(myThread, self).__init__()
    
    def setAnswer(self, value):
        self._answer = value
    
    def setAnswerFlag(self):
        self._answer_flag = True
    
    def run(self):
        self._answer_flag = False
        time.sleep(self.__ANSWER_SHOW_TIME_S)
        if self._answer_flag == False:
            print(self._answer)




# 関数とクラスの使い分けってどうするのが正しいのかよくわからない。
# リストを出力して選択させる関数

def showList(ordered_dict):
    while True:
        for key, value in ordered_dict.items():
            print(key + "\t" + value)
        
        print("上記左記の数字を入力してね")
        select = input()
        
        # 入力値がリストに含まれるかの確認
        if select in ordered_dict.keys():
            return select
        else:
            print("ちゃんと書いてある数字いれてよ。")
            print()


# メイン処理だよーん
if __name__ == '__main__':
    db      = DB()
    db_list = db.getDbList()
    db_id = showList(db_list)
    db_name = db_id + "_" + db_list[db_id]
    

    choice_list = Choice.getChoiceList()
    choice_id   = showList(choice_list)
    
    order_list  = Order.getOrderList()
    order_id    = showList(order_list)
    print()

    
    question = Question(db.getConnection(db_name), choice_id, order_id)
    question.start()
