#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sqlite3
import codecs
import csv
from collections import OrderedDict

PATH = "../"
CREATE_TABLE = """
    create table Question(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        jp TEXT NOT NULL,
        en TEXT NOT NULL
    )"""
INSERT_SQL = "INSERT INTO Question(en, jp) VALUES(?, ?)"

def getTsvList():
    tsv_list = OrderedDict()
    for x in os.listdir("./"):
        if os.path.isfile(x) and ".tsv" in x:
            # 数字と文字を分割
            words = x.replace("_", " ").split(" ")
            tsv_list[words[0]] = words[1]
    return tsv_list

# リストを出力して選択させる関数
# リストを出力して選択させる関数
def showTsvList(ordered_dict):
    while True:
        for key, value in ordered_dict.items():
            print(key + "\t" + value)
        
        print("上記左記の数字を入力してね")
        select = input()
        
        # 入力値がリストに含まれるかの確認
        if select in ordered_dict.keys():
            return select
        else:
            print("ちゃんと書いてある数字いれてよ。")




if __name__ == '__main__':
    tsv_id = showTsvList(getTsvList())
    tsv_name = tsv_id + "_" + getTsvList()[tsv_id]
    
    f       = codecs.open(tsv_name, 'r', 'utf-8')
    reader  = csv.reader(f, delimiter = '\t')
    
    print(PATH + tsv_name.replace(".tsv", ".db"))
    connection  = sqlite3.connect(PATH + tsv_name.replace(".tsv", ".db"))
    cursor      = connection.cursor()
    # テーブル除去
    try:
        cursor.execute("DROP TABLE Question")
    except:
        pass
    
    # テーブル作成
    cursor.execute(CREATE_TABLE)
    
    for row in reader:
        cursor.execute(INSERT_SQL, row)

    # コミットだよ
    cursor.execute("COMMIT")
    
    # 空き容量の解放だよ
    cursor.execute("VACUUM")
    connection.commit()
    f.close()